package com.cubicequation.autokey_akeylang;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public final class Parser
{
    private Parser()
    {
        throw new IllegalStateException("Parser cannot be instantiated");
    }

    public static @NotNull Node[] parseTokens(final Token @NotNull [][] tokens) throws AKeyException
    {
        ArrayList<Node> roots = new ArrayList<>();

        for (final Token[] line : tokens)
            switch (line[0].type())
            {
                case IDENTIFIER ->
                {
                    if (line.length < 2) throw AKeyException.missingTokenException(line[0]);
                    if (line[1].type() != Token.Type.OPENING_PARENTHESIS && line[1].type() != Token.Type.EQUALS)
                        throw AKeyException.unexpectedTokenException(line[1]);
                    roots.add(Evaluator.evaluateLine(line, 0));
                }
                case VAR ->
                {
                    if (line.length < 3) throw AKeyException.missingTokenException(line[0]);
                    final Node node = new Node(line[0], Node.Type.VAR);
                    node.addChild(Evaluator.evaluateLine(line, 1));
                    roots.add(node);
                }
                case RETURN -> roots.add(new Node(line[0], Node.Type.RETURN));
                case IF -> roots.add(parseIf(line));
                case WHILE -> roots.add(parseWhile(line));
                case FUNCTION -> roots.add(new Node(line[1], Node.Type.FUNCTION));
                case CLOSE -> roots.add(new Node(line[0], Node.Type.CLOSE));
                default -> throw AKeyException.unexpectedTokenException(line[0]);
            }

        return roots.toArray(Node[]::new);
    }

    private static @NotNull Node parseIf(final @NotNull Token @NotNull [] line) throws AKeyException
    {
        final Node root = new Node(line[0], Node.Type.IF);
        root.addChild(Evaluator.evaluateLine(line, 1));
        return root;
    }

    private static @NotNull Node parseWhile(final @NotNull Token @NotNull [] line) throws AKeyException
    {
        final Node root = new Node(line[0], Node.Type.WHILE);
        root.addChild(Evaluator.evaluateLine(line, 1));
        return root;
    }
}
