package com.cubicequation.autokey_akeylang;

import com.cubicequation.autokey_core.language.exceptions.CompilerException;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public final class AKeyException extends CompilerException
{
    public AKeyException(final @NotNull String message, final @NotNull Token token, final @NotNull Object @NotNull ... data)
    {
        super(message, data);
        this.line = token.line();
        this.position = token.start();
    }

    @Contract("_ -> new")
    public static @NotNull AKeyException unexpectedTokenException(final @NotNull Token token)
    {
        return new AKeyException("autokey_core.language.exception.unexpected_token", token, token.value(), token.type());
    }

    @Contract("_ -> new")
    public static @NotNull AKeyException missingTokenException(final @NotNull Token token)
    {
        return new AKeyException("autokey_core.language.exception.missing_token", token);
    }
}