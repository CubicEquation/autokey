package com.cubicequation.autokey_akeylang;

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public final class Compiler implements com.cubicequation.autokey_core.language.Compiler
{
    @Override
    public @NotNull String compile(final @NotNull Scanner scanner) throws AKeyException
    {
        final Token[][] tokens = Lexer.getTokens(scanner);
        final Node[] trees = Parser.parseTokens(tokens);
        return "!version=1!\n" + Converter.convert(trees);
    }
}
