package com.cubicequation.autokey_akeylang.entrypoints;

import com.cubicequation.autokey_core.api.CompilerAPI;
import com.cubicequation.autokey_core.language.Compiler;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class AKeyCompiler implements CompilerAPI
{
    @Override
    public @NotNull Map<String, Compiler> getCompilers()
    {
        final HashMap<String, Compiler> compilers = new HashMap<>();
        compilers.put("akey", new com.cubicequation.autokey_akeylang.Compiler());
        return compilers;
    }
}
