package com.cubicequation.autokey_core.language.aasm;

import com.cubicequation.autokey_core.language.aasm.v0.VM;
import com.cubicequation.autokey_core.language.exceptions.AutokeyException;
import com.cubicequation.autokey_core.language.exceptions.RuntimeException;
import com.cubicequation.autokey_core.util.Feedback;
import com.cubicequation.autokey_core.util.TickScheduler;
import net.minecraft.text.HoverEvent;
import net.minecraft.text.MutableText;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Scanner;

public final class AasmVM
{
    private static final MutableText VERSION_PARSE_ERROR_TEXT = Text.translatable("autokey_core.language.aasm.version_parse_error");
    private static final MutableText UNSUPPORTED_VERSION_TEXT = Text.translatable("autokey_core.language.aasm.unsupported_version");
    private static final MutableText RUNNING_TEXT = Text.translatable("autokey_core.language.aasm.running");
    private static final MutableText EXECUTION_ERROR_TEXT = Text.translatable("autokey_core.language.aasm.execution_error");
    private static final MutableText EXECUTION_JAVA_ERROR_TEXT = Text.translatable("autokey_core.language.aasm.java_error");
    private static final MutableText EXECUTION_RUNTIME_ERROR_TEXT = Text.translatable("autokey_core.language.aasm.runtime_error");
    private static final MutableText EXECUTION_FINISHED_TEXT = Text.translatable("autokey_core.language.aasm.execution_finished");
    private static final MutableText NOT_RUNNING_TEXT = Text.translatable("autokey_core.language.aasm.not_running");
    private static final MutableText CANCELED_TEXT = Text.translatable("autokey_core.language.aasm.canceled");
    private final IVM vm;
    private final String name;
    private TickScheduler.TickScheduleable scheduleable;

    public AasmVM(final @NotNull Scanner aasm, final @NotNull String name)
    {
        final Flags flags = new Flags(aasm);

        final String version = flags.getFlagValue("version")
                .orElseGet(() ->
                {
                    Feedback.sendClientMessage(VERSION_PARSE_ERROR_TEXT, name);
                    return "1";
                });

        vm = switch (version)
        {
            case "1" -> new VM(aasm);
            default ->
            {
                Feedback.sendClientMessage(UNSUPPORTED_VERSION_TEXT, name, version);
                yield new VM(aasm);
            }
        };

        aasm.close();
        this.name = name;
    }

    public void run(final boolean silent, final @Nullable Environment environment, final @Nullable Runnable finishAction)
    {
        if (!silent) Feedback.sendClientMessage(RUNNING_TEXT, name);

        scheduleable = TickScheduler.schedule(runnable ->
        {
            try
            {
                vm.run(environment == null ? new Environment(new HashMap<>()) : environment);
            } catch (Exception e)
            {
                final MutableText errorMessage;

                if (e instanceof AasmException)
                {
                    errorMessage = EXECUTION_ERROR_TEXT;
                }
                else if (e instanceof RuntimeException)
                {
                    errorMessage = EXECUTION_RUNTIME_ERROR_TEXT;
                }
                else
                {
                    errorMessage = EXECUTION_JAVA_ERROR_TEXT;
                }

                final Style hoverEvent = Style.EMPTY.withHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, Text.literal(AutokeyException.prettyPrint(e))));
                Feedback.sendClientErrorMessage(errorMessage.setStyle(hoverEvent), e, name);
            }

            if (!(silent || runnable.hasExited())) Feedback.sendClientMessage(EXECUTION_FINISHED_TEXT, name);

            if (finishAction != null) finishAction.run();
        }, true, 5);
    }

    public void interrupt()
    {
        if (scheduleable == null || scheduleable.hasExited())
        {
            Feedback.sendClientMessage(NOT_RUNNING_TEXT, name);
            return;
        }

        vm.interrupt();
        scheduleable.interrupt();
        scheduleable = null;

        Feedback.sendClientMessage(CANCELED_TEXT, name);
    }

    public boolean isRunning()
    {
        return !(scheduleable == null || scheduleable.hasExited());
    }
}
