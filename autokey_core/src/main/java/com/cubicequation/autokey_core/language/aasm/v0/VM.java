package com.cubicequation.autokey_core.language.aasm.v0;

import com.cubicequation.autokey_core.language.aasm.Environment;
import com.cubicequation.autokey_core.language.aasm.IVM;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public final class VM implements IVM
{
    private final Lexer.LexerData lexerData;
    public VM(final @NotNull Scanner aasm)
    {
        lexerData = Lexer.getTokens(aasm);
    }

    private Executor.Interrupter interrupter;

    @Override
    public void run(final @NotNull Environment environment) throws Exception
    {
        interrupter = new Executor.Interrupter();
        Executor.execute(lexerData.lines(), lexerData.labels(), environment, interrupter);
    }

    @Override
    public void interrupt()
    {
        interrupter.interrupt();
    }
}
