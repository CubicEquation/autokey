package com.cubicequation.autokey_core.language.aasm.v0;

enum TokenType
{
    IDENTIFIER,
    VAR,
    DATA,
    FREE,
    JUMPIF,
    JUMP,
    OUTPUT,
    LABEL,
    SET,
    NEW_STACK,
    STACK_AMOUNT,
    STACK
}
