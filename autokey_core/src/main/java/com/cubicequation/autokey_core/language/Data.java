package com.cubicequation.autokey_core.language;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public final class Data
{
    @Contract("_ -> new")
    public static @NotNull Object fromString(@NotNull final String str)
    {
        if (str.startsWith("\"") && str.endsWith("\"") && str.length() > 1)
            return str.substring(1, str.length() - 1);
        else if (str.equals("true")) return true;
        else if (str.equals("false")) return false;

        try
        {
            return Integer.parseInt(str);
        } catch (NumberFormatException ignored)
        {
        }

        try
        {
            return Double.parseDouble(str);
        } catch (NumberFormatException ignored)
        {
        }

        try
        {
            return Float.parseFloat(str);
        } catch (NumberFormatException ignored)
        {
        }

        return str;
    }

    public static boolean isData(final @NotNull String str)
    {
        if (str.startsWith("\"") && str.endsWith("\"") && str.length() > 1)
            return true;
        else if (str.equals("true") || str.equals("false")) return true;

        try
        {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException ignored)
        {
        }

        try
        {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException ignored)
        {
        }

        try
        {
            Float.parseFloat(str);
            return true;
        } catch (NumberFormatException ignored)
        {
        }

        return false;
    }
}
