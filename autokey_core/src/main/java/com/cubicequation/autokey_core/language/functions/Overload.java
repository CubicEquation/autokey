package com.cubicequation.autokey_core.language.functions;

import com.cubicequation.autokey_core.language.exceptions.RuntimeException;
import com.cubicequation.autokey_core.language.functions.runnables.Runnable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class Overload
{
    private final Runnable runnable;
    private final @Nullable Class<?> returnType;
    private final @NotNull Class<?> @NotNull [] argumentTypes;

    public Overload(final @NotNull Runnable runnable, final @Nullable Class<?> returnType, final @NotNull Class<?> @NotNull [] argumentTypes)
    {
        this.runnable = runnable;
        this.returnType = returnType;
        this.argumentTypes = argumentTypes;
    }

    public final @NotNull Optional<Boolean> matches(final Class<?> @NotNull [] argumentTypes)
    {
        if (this.argumentTypes.length != argumentTypes.length) return Optional.empty();
        if (this.argumentTypes == argumentTypes) return Optional.of(true);

        for (int i = 0; i < argumentTypes.length; i++)
            if (!this.argumentTypes[i].isAssignableFrom(argumentTypes[i]) && !(Number.class.isAssignableFrom(this.argumentTypes[i]) && Number.class.isAssignableFrom(argumentTypes[i])))
                return Optional.empty();

        return Optional.of(false);
    }

    public final @NotNull Optional<Object> run(final @NotNull Object @NotNull [] arguments) throws RuntimeException
    {
        return runnable.run(arguments);
    }

    public final @Nullable Class<?> getReturnType()
    {
        return returnType;
    }

    public final @NotNull Class<?> @NotNull [] getArgumentTypes()
    {
        return argumentTypes;
    }
}
