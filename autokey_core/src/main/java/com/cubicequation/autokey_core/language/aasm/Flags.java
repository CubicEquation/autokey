package com.cubicequation.autokey_core.language.aasm;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Flags
{
    private static final Pattern FLAG_PATTERN = Pattern.compile("!([a-zA-Z0-9]+)(?:=([a-zA-Z0-9]+))?!");
    private final HashMap<String, String> flags;

    public Flags(final @NotNull Scanner scanner)
    {
        flags = new HashMap<>();
        try
        {
            while (scanner.hasNext(FLAG_PATTERN))
            {
                Matcher matcher = FLAG_PATTERN.matcher(scanner.next());
                matcher.find();
                flags.put(matcher.group(1), matcher.group(2));
            }
        } catch (NoSuchElementException ignored) { }
    }

    public boolean hasFlag(final @NotNull String flag)
    {
        return flags.containsKey(flag);
    }

    public @NotNull Optional<String> getFlagValue(String flag)
    {
        final String value = flags.get(flag);

        if (value == null) return Optional.empty();

        return Optional.of(value);
    }
}
