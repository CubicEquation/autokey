package com.cubicequation.autokey_core.language.exceptions;

import org.jetbrains.annotations.NotNull;

public class RuntimeException extends AutokeyException
{
    public RuntimeException(final @NotNull String message)
    {
        super(message);
    }

    public RuntimeException(final @NotNull String message, final @NotNull Object @NotNull ... data)
    {
        super(message, data);
    }

    public void setLocation(final int line, final int position)
    {
        this.line = line;
        this.position = position;
    }
}