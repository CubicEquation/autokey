package com.cubicequation.autokey_core.gui;

import com.cubicequation.autokey_core.file.File;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.Drawable;
import net.minecraft.client.gui.Element;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;

public class MenuEntry implements Drawable, Element
{
    private static final Identifier TEXTURE;

    static
    {
        TEXTURE = new Identifier("autokey_core:textures/gui/menu_entry.png");
    }

    private final int x;
    private final int y;
    private final int textX;
    private final int textY;
    private final CustomButtonWidget executeButton;
    private final CustomButtonWidget cancelButton;
    private final MutableText name;
    private final File file;
    private boolean focused = false;

    public MenuEntry(int x, int y, @NotNull File file)
    {
        this.x = x;
        this.y = y;

        textX = x + 4;
        textY = y + 4;

        this.file = file;

        name = Text.literal(file.getName());
        executeButton = new CustomButtonWidget(x + 105, y + 4, TEXTURE, 0, 34, 8, 34, 16, 34, 8, file.getExecutionManager()::execute);
        cancelButton = new CustomButtonWidget(x + 105, y + 4, TEXTURE, 0, 42, 8, 42, 16, 42, 8, file.getExecutionManager()::cancel);

        file.getUIManager()
                .setMenuEntryCallback(this::updateState);
        updateState();
    }

    public void render(@NotNull DrawContext context, int mouseX, int mouseY, float delta)
    {
        context.drawTexture(TEXTURE, x, y, 0, 0, 117, 17);

        executeButton.render(context, mouseX, mouseY, delta);
        cancelButton.render(context, mouseX, mouseY, delta);

        context.drawTextWithShadow(MinecraftClient.getInstance().textRenderer, name, textX, textY, -1);
    }

    public boolean mouseClicked(double mouseX, double mouseY, int button)
    {
        switch (file.getStateManager()
                .getExecutionState())
        {
            case EXECUTABLE ->
            {
                if (executeButton.mouseClicked(mouseX, mouseY, button))
                {
                    executeButton.setFocused(true);
                    return true;
                }
                executeButton.setFocused(false);
            }
            case CANCELABLE ->
            {
                if (cancelButton.mouseClicked(mouseX, mouseY, button))
                {
                    cancelButton.setFocused(true);
                    return true;
                }
                cancelButton.setFocused(false);
            }
        }

        if (mouseX >= x && mouseY >= y && mouseX < x + 117 && mouseY < y + 17)
        {
            MinecraftClient.getInstance()
                    .setScreen(new SettingsScreen(file));
            return true;
        }

        return false;
    }

    @Override
    public boolean isFocused()
    {
        return this.focused;
    }

    @Override
    public void setFocused(boolean focused)
    {
        this.focused = focused;
    }

    private void updateState()
    {
        executeButton.setVisibility(false);
        cancelButton.setVisibility(false);

        switch (file.getStateManager()
                .getExecutionState())
        {
            case EXECUTABLE -> executeButton.setVisibility(true);
            case CANCELABLE -> cancelButton.setVisibility(true);
        }
    }
}
