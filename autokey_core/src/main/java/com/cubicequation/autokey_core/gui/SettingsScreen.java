package com.cubicequation.autokey_core.gui;

import com.cubicequation.autokey_core.entrypoints.Autokey;
import com.cubicequation.autokey_core.file.CompilingState;
import com.cubicequation.autokey_core.file.File;
import com.cubicequation.autokey_core.language.Languages;
import com.cubicequation.autokey_core.util.Feedback;
import com.cubicequation.autokey_core.util.Numbers;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.glfw.GLFW;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

public class SettingsScreen extends Screen
{
    private static final Identifier TEXTURE;
    private static final int BACKGROUND_WIDTH;
    private static final int BACKGROUND_HEIGHT;
    private static final Text ON_CHAT_INPUT_BUTTON_NAME;
    private static final Text ONLY_ALLOW_CLIENT_MESSAGES_NAME;
    private static final MutableText PROPERTY_SAVE_SUCCESS = Text.translatable("gui.autokey_core.settings_screen.property_save_success");
    private static final MutableText PROPERTY_LOAD_SUCCESS = Text.translatable("gui.autokey_core.settings_screen.property_load_success");
    private static final MutableText ACTION_NUMBER_FIELD_NAME_NORMAL = Text.translatable("gui.autokey_core.settings_screen.action_number_field_headline");
    private static final MutableText ACTION_NUMBER_FIELD_NAME_WARNING = Text.translatable("gui.autokey_core.settings_screen.action_number_field_headline_red");
    private static final MutableText COMPILER_FIELD_NAME_NORMAL = Text.translatable("gui.autokey_core.settings_screen.compiler_field_headline");
    private static final MutableText COMPILER_FIELD_NAME_WARNING = Text.translatable("gui.autokey_core.settings_screen.compiler_field_headline_red");

    static
    {
        TEXTURE = new Identifier("autokey_core:textures/gui/settings.png");

        BACKGROUND_WIDTH = 147;
        BACKGROUND_HEIGHT = 168;

        ON_CHAT_INPUT_BUTTON_NAME = Text.translatable("gui.autokey_core.settings_screen.execute_on_message");

        ONLY_ALLOW_CLIENT_MESSAGES_NAME = Text.translatable("gui.autokey_core.settings_screen.only_allow_client_messages");
    }

    private final Text fileName;
    private final File file;
    private Text compilerFieldName;
    private Text actionNumberFieldName;
    private int x;
    private int y;
    private TextFieldWidget compilerField;
    private TextFieldWidget actionNumberField;
    private TextFieldWidget prefixField;
    private CustomToggleButtonWidget onChatInputButton;
    private CustomToggleButtonWidget onlyAllowClientMessagesButton;
    private CustomButtonWidget saveButton;
    private CustomButtonWidget loadButton;
    private CustomButtonWidget compileButton;

    public SettingsScreen(@NotNull File file)
    {
        super(Text.translatable("gui.autokey_core.settings_screen.title"));
        this.file = file;
        fileName = Text.literal("§l§n" + file.getName());
    }

    protected void init()
    {
        x = (width - BACKGROUND_WIDTH) / 2;
        y = (height - BACKGROUND_HEIGHT) / 2;

        saveButton = new CustomButtonWidget(x + 107, y + 12, TEXTURE, 0, 180, 12, 180, 24, 180, 12, () ->
        {
            if (file.getPropertyManager()
                    .save())
                Feedback.sendClientMessage(PROPERTY_SAVE_SUCCESS, file.getName());
        });
        loadButton = new CustomButtonWidget(x + 123, y + 12, TEXTURE, 0, 192, 12, 192, 24, 192, 12, () ->
        {
            if (!file.getPropertyManager()
                    .load()) return;

            compilerField.setText(file.getPropertyManager()
                    .getProperty("compiler"));
            prefixField.setText(file.getPropertyManager()
                    .getProperty("chatPrefix"));
            actionNumberField.setText(file.getPropertyManager()
                    .getProperty("actionKey"));
            onChatInputButton.setToggled(Objects.equals(file.getPropertyManager()
                    .getProperty("chatSubscribed"), "true"));
            onlyAllowClientMessagesButton.setToggled(Objects.equals(file.getPropertyManager()
                    .getProperty("onlyAllowClientMessages"), "true"));

            refreshCompilerResults();
            refreshActionNumberField();

            Feedback.sendClientMessage(PROPERTY_LOAD_SUCCESS, file.getName());
        });

        compilerField = new TextFieldWidget(MinecraftClient.getInstance().textRenderer, x + 13, y + 42, 105, 14, Text.translatable("gui.autokey_core.settings_screen.compiler_field_name"));
        compilerField.setMaxLength(75);
        compilerField.setDrawsBackground(true);
        compilerField.setText(file.getPropertyManager()
                .getProperty("compiler"));

        compileButton = new CustomButtonWidget(x + 123, y + 43, TEXTURE, 0, 168, 12, 168, 24, 168, 12, file::compile);

        actionNumberField = new TextFieldWidget(MinecraftClient.getInstance().textRenderer, x + 13, y + 77, 105, 14, Text.translatable("gui.autokey_core.settings_screen.action_number_field_name"));
        actionNumberField.setMaxLength(75);
        actionNumberField.setDrawsBackground(true);
        actionNumberField.setText(file.getPropertyManager()
                .getProperty("actionKey"));

        prefixField = new TextFieldWidget(MinecraftClient.getInstance().textRenderer, x + 30, y + 112, 88, 14, Text.translatable("gui.autokey_core.settings_screen.prefix_field_name"));
        prefixField.setMaxLength(75);
        prefixField.setDrawsBackground(true);
        prefixField.setText(file.getPropertyManager()
                .getProperty("chatPrefix"));

        onChatInputButton = new CustomToggleButtonWidget(x + 13, y + 113, Objects.equals(file.getPropertyManager()
                .getProperty("chatSubscribed"), "true"), value -> file.getPropertyManager()
                .setProperty("chatSubscribed", String.valueOf(value)));

        onlyAllowClientMessagesButton = new CustomToggleButtonWidget(x + 13, y + 148, Objects.equals(file.getPropertyManager()
                .getProperty("onlyAllowClientMessages"), "true"), value -> file.getPropertyManager()
                .setProperty("onlyAllowClientMessages", String.valueOf(value)));

        file.getUIManager()
                .setSettingsScreenCallback(this::refreshCompilerResults);
        refreshCompilerResults();
        refreshActionNumberField();
    }

    public boolean mouseClicked(double mouseX, double mouseY, int button)
    {
        if (actionNumberField.mouseClicked(mouseX, mouseY, button))
        {
            actionNumberField.setFocused(true);
            return true;
        }
        else actionNumberField.setFocused(false);

        if (compilerField.mouseClicked(mouseX, mouseY, button))
        {
            compilerField.setFocused(true);
            return true;
        }
        else compilerField.setFocused(false);

        if (prefixField.mouseClicked(mouseX, mouseY, button))
        {
            prefixField.setFocused(true);
            return true;
        }
        else prefixField.setFocused(false);

        if (compileButton.mouseClicked(mouseX, mouseY, button))
        {
            compileButton.setFocused(true);
            return true;
        }
        else compileButton.setFocused(false);

        if (saveButton.mouseClicked(mouseX, mouseY, button))
        {
            saveButton.setFocused(true);
            return true;
        }
        else saveButton.setFocused(false);

        if (saveButton.mouseClicked(mouseX, mouseY, button))
        {
            saveButton.setFocused(true);
            return true;
        }
        else saveButton.setFocused(false);

        if (onChatInputButton.mouseClicked(mouseX, mouseY, button))
        {
            onChatInputButton.setFocused(true);
            return true;
        }
        else onChatInputButton.setFocused(false);

        if (onlyAllowClientMessagesButton.mouseClicked(mouseX, mouseY, button))
        {
            onlyAllowClientMessagesButton.setFocused(true);
            return true;
        }
        else onlyAllowClientMessagesButton.setFocused(false);

        return false;
    }

    public boolean charTyped(char chr, int modifiers)
    {
        if (compilerField.charTyped(chr, modifiers))
        {
            refreshCompilerResults();
            compilerField.setFocused(true);
            return true;
        }
        else compilerField.setFocused(false);

        if (actionNumberField.charTyped(chr, modifiers))
        {
            refreshCompilerResults();
            actionNumberField.setFocused(true);
            return true;
        }
        else actionNumberField.setFocused(false);

        if (prefixField.charTyped(chr, modifiers))
        {
            file.getPropertyManager()
                    .setProperty("chatPrefix", prefixField.getText());
            prefixField.setFocused(true);
            return true;
        }
        else prefixField.setFocused(false);

        return false;
    }

    public boolean keyPressed(int keyCode, int scanCode, int modifiers)
    {
        if (compilerField.keyPressed(keyCode, scanCode, modifiers) || compilerField.isFocused())
        {
            refreshCompilerResults();
            compilerField.setFocused(true);
            return true;
        }
        else compilerField.setFocused(false);

        if (actionNumberField.keyPressed(keyCode, scanCode, modifiers) || actionNumberField.isFocused())
        {
            refreshActionNumberField();
            actionNumberField.setFocused(true);
            return true;
        }
        else actionNumberField.setFocused(false);

        if (prefixField.keyPressed(keyCode, scanCode, modifiers) || prefixField.isFocused())
        {
            file.getPropertyManager()
                    .setProperty("chatPrefix", prefixField.getText());
            prefixField.setFocused(true);
            return true;
        }
        else prefixField.setFocused(false);

        if (keyCode == GLFW.GLFW_KEY_ESCAPE)
        {
            MinecraftClient.getInstance()
                    .setScreen(new MenuScreen());
            return true;
        }

        return false;
    }

    public boolean keyReleased(int keyCode, int scanCode, int modifiers)
    {
        if (compilerField.keyReleased(keyCode, scanCode, modifiers) || compilerField.isFocused())
        {
            compilerField.setFocused(true);
            return true;
        }
        else compilerField.setFocused(false);

        if (actionNumberField.keyReleased(keyCode, scanCode, modifiers) || actionNumberField.isFocused())
        {
            actionNumberField.setFocused(true);
            return true;
        }
        else actionNumberField.setFocused(false);

        if (prefixField.keyReleased(keyCode, scanCode, modifiers) || prefixField.isFocused())
        {
            prefixField.setFocused(true);
            return true;
        }
        else prefixField.setFocused(false);

        return false;
    }

    public void render(DrawContext context, int mouseX, int mouseY, float delta)
    {
        //Render dark background
        this.renderBackground(context, mouseX, mouseY, delta);

        super.render(context, mouseX, mouseY, delta);

        //Render UI background
        drawBackground(context);

        final TextRenderer textRenderer = MinecraftClient.getInstance().textRenderer;

        //Title
        context.drawTextWithShadow(textRenderer, fileName, x + 13, y + 13, -1);
        saveButton.render(context, mouseX, mouseY, delta);
        loadButton.render(context, mouseX, mouseY, delta);

        //Compiling
        context.drawTextWithShadow(textRenderer, compilerFieldName, x + 13, y + 30, -1);
        compilerField.render(context, mouseX, mouseY, delta);
        compileButton.render(context, mouseX, mouseY, delta);

        //Action number field
        context.drawTextWithShadow(textRenderer, actionNumberFieldName, x + 13, y + 65, -1);
        actionNumberField.render(context, mouseX, mouseY, delta);

        //Chat input Button
        context.drawTextWithShadow(textRenderer, ON_CHAT_INPUT_BUTTON_NAME, x + 13, y + 100, -1);
        onChatInputButton.render(context, mouseX, mouseY, delta);
        prefixField.render(context, mouseX, mouseY, delta);

        //Only allow client messages
        context.drawTextWithShadow(textRenderer, ONLY_ALLOW_CLIENT_MESSAGES_NAME, x + 13, y + 135, -1);
        onlyAllowClientMessagesButton.render(context, mouseX, mouseY, delta);
    }

    private void drawBackground(@NotNull DrawContext context)
    {
        context.drawTexture(TEXTURE, x, y, 0, 0, BACKGROUND_WIDTH, BACKGROUND_HEIGHT);
    }

    private void refreshActionNumberField()
    {
        final Optional<Integer> oldNumber = Numbers.parseInt(file.getPropertyManager()
                .getProperty("actionKey"));
        if (oldNumber.isPresent() && oldNumber.get() < 10 && oldNumber.get() > 0)
            Autokey.setAction(oldNumber.get() - 1, null);

        final Optional<Integer> id = Numbers.parseInt(actionNumberField.getText()
                .trim());
        if (id.isPresent() && id.get() > 0 && id.get() < 10)
        {
            actionNumberFieldName = ACTION_NUMBER_FIELD_NAME_NORMAL;
            Autokey.setAction(id.get() - 1, file);
        }
        else actionNumberFieldName = ACTION_NUMBER_FIELD_NAME_WARNING;

        file.getPropertyManager()
                .setProperty("actionKey", String.valueOf(id.orElse(0)));
    }

    private void refreshCompilerResults()
    {
        final String compilerText = compilerField.getText()
                .trim();
        file.getPropertyManager()
                .setProperty("compiler", compilerText);

        if (Arrays.asList(file.getLanguages())
                .contains(compilerText) && Languages.hasCompiler(compilerText))
        {
            compilerFieldName = COMPILER_FIELD_NAME_NORMAL;
            if (file.getStateManager()
                    .getCompilingState() != CompilingState.COMPILING) compileButton.enabled(true);
        }
        else
        {
            compileButton.enabled(false);
            compilerFieldName = COMPILER_FIELD_NAME_WARNING;
        }
    }
}