package com.cubicequation.autokey_core.util;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public final class StringUtil
{
    private StringUtil()
    {
        throw new IllegalStateException("StringUtil cannot be instantiated");
    }

    public static Integer @NotNull [] getSpacePositions(final @NotNull CharSequence string)
    {
        final ArrayList<Integer> spacePositions = new ArrayList<>();

        for (int i = 0; i < string.length(); i++)
        {
            if (string.charAt(i) == ' ')
                spacePositions.add(i);
        }

        return spacePositions.toArray(new Integer[0]);
    }
}
