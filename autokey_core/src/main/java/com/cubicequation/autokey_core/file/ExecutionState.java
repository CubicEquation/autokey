package com.cubicequation.autokey_core.file;

public enum ExecutionState
{
    EXECUTABLE,
    CANCELABLE,
    NOT_EXECUTABLE
}
