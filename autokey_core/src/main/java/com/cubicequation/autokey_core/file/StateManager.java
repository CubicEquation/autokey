package com.cubicequation.autokey_core.file;

import org.jetbrains.annotations.NotNull;

public final class StateManager
{
    private final File file;
    private CompilingState compilingState;
    private ExecutionState executionState;

    StateManager(final @NotNull File file)
    {
        this.file = file;
    }

    void setCompiling()
    {
        compilingState = CompilingState.COMPILING;
        file.getUIManager().invokeSettingsScreenCallback();
    }

    void updateCompilingState()
    {
        compilingState = new java.io.File(file.getFile(), "aasm").isFile() ? CompilingState.COMPILED : CompilingState.UN_COMPILED;
        file.getUIManager().invokeSettingsScreenCallback();
    }

    void updateExecutionState()
    {
        if (compilingState == CompilingState.COMPILED)
            executionState = file.getExecutionManager().isExecuting() ? ExecutionState.CANCELABLE : ExecutionState.EXECUTABLE;
        else executionState = ExecutionState.NOT_EXECUTABLE;

        file.getUIManager().invokeMenuEntryCallback();
    }

    public CompilingState getCompilingState()
    {
        return compilingState;
    }

    public ExecutionState getExecutionState()
    {
        return executionState;
    }
}
