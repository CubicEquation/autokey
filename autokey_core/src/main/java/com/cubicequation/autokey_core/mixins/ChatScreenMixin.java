package com.cubicequation.autokey_core.mixins;

import com.cubicequation.autokey_core.file.ExecutionManager;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ChatScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ChatScreen.class)
public abstract class ChatScreenMixin
{
    @Inject(method = "sendMessage", at = @At("HEAD"), cancellable = true)
    private void sendMessage(String chatText, boolean addToHistory, CallbackInfo ci)
    {
        if (ExecutionManager.receivedClientMessage(chatText))
        {
            if (addToHistory) MinecraftClient.getInstance().inGameHud.getChatHud()
                    .addToMessageHistory(chatText);
            ci.cancel();
        }
    }
}
