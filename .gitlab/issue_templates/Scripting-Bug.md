## Summary

(Summarize the bug encountered concisely)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## Example Script(s)

(Paste the content of the .aasm file here.  
If you didn't code using aasm also paste the content of the main file here.  
Please use code blocks (```) to format them, as it's very hard to read otherwise)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format them as it's very hard to read otherwise.  
You can find the Minecraft logs in the .minecraft/logs folder)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)
